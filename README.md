# Refactored Automation Framework

## Things Done

1. Updated `pom.xml` dependencies with new ones (where applicable) and removed unused ones.
2. Created a new package named `serenitySteps` where I've moved all the logic.
3. In the `stepDefinitions` package, I've left just the steps that will be used in feature files.
4. Modified the `features.search` feature file to display exactly what the scenarios will do.
5. Added new scenarios in the `GetProducts.feature` file.
6. Modified `TestRunner` to get all the sub-packages from features.
7. Created `.gitlab-ci.yml` for Gitlab CI integration.
8. Splitting resources files in their correct packages

## Getting Started

To start the project on your local environment:

1. Clone the project on your machine from [https://gitlab.com/alexandru-dan/cucumber-refactor-project.git](https://gitlab.com/alexandru-dan/lease-plan.git).
2. Ensure Java 17 is installed on your local machine.
3. Build the project using Maven.
4. You can start scenarios right from the `GetProducts.feature` file.
5. If you want to start the scenarios with Maven, use the following command:  "mvn clean verify"
6. To check the report -> navigate to target/site/serenity/index.html and open it with a browser(i.e Google Chrome, Firefox, etc)

## GitLab CI Integration

To check the pipeline and the report on Gitlab CI:
1. Navigate to [https://gitlab.com/alexandru-dan/cucumber-refactor-project/-/pipelines](..)
2. Select a pipeline or start a new one
3. After it's finished, click on `Status`
4. Click on `test` job
5. In the right part of the page you will see `Job artifacts`
6. You can keep, download or browse inside the reporting folder
7. When you open index.html from target/site/serenity/ you can check the report