package steps.serenitySteps;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.serenitybdd.annotations.Step;
import net.serenitybdd.rest.SerenityRest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;

public class ProductsAPI {

    @Step
    public void callEndpoint(String endpoint) {
         SerenityRest.given().contentType(ContentType.JSON).get(endpoint);
    }

    @Step
    public void verifyStatusCode(int statusCode) {
        SerenityRest.then().statusCode(statusCode);
    }

    @Step
    public void verifyResultsDisplayedForProductInResponseOnASpecificKey(String wordExpected, String key) {
        SerenityRest.then().body(key, everyItem(containsStringIgnoringCase(wordExpected)));
    }

    @Step
    public void verifyResultsDisplayedForProductInResponse(){
        Response response = SerenityRest.then().extract().response();
        String responseBody = response.getBody().asString();
        assertThat(responseBody).as("The response doesn't have products or is null")
                .isNotNull().isNotEqualTo("[]");
    }

    @Step
    public void verifyErrorAndMessageDisplayed(String message) {
        SerenityRest.then().body("detail.error", equalTo(true));
        SerenityRest.then().body("detail.message", containsStringIgnoringCase(message));
    }

    @Step
    public void notAuthenticatedErrorIsDisplayed(String message){
        SerenityRest.then().body("detail", containsString(message));
    }

    @Step
    public void emptyListIsDisplayed(){
        Response response = SerenityRest.then().extract().response();
        assertThat(response.getBody().asString()).isEqualTo("[]");
    }
}
