package steps.stepDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.annotations.Steps;
import net.serenitybdd.core.Serenity;
import steps.serenitySteps.ProductsAPI;


public class SearchStepDefinitions {

    @Steps
    public ProductsAPI productsAPI;

    @When("the user search for {string}")
    public void heCallsEndpoint(String product) {
        productsAPI.callEndpoint(Serenity.sessionVariableCalled("endpoint") + product);
    }

    @Then("he sees the results that contains {string} in the {string}")
    public void heSeesTheResultsDisplayedForApple(String product, String jsonObjectWhereToSearch) {
        productsAPI.verifyResultsDisplayedForProductInResponseOnASpecificKey(product, jsonObjectWhereToSearch);
    }

    @Then("an error is displayed with the message {string}")
    public void anErrorIsDisplayed(String message){
        productsAPI.verifyErrorAndMessageDisplayed(message);
    }

    @Then("{string} error is displayed")
    public void notAuthenticatedErrorIsDisplayed(String error){
        productsAPI.notAuthenticatedErrorIsDisplayed(error);
    }

    @Given("the user wants to use endpoint {string} to get products")
    public void theUserWantsToUseEndpointToGetProducts(String endpoint_) {
        Serenity.setSessionVariable("endpoint").to(endpoint_);
    }

    @Then("an empty list will be displayed")
    public void anEmptyListWillBeDisplayed() {
        productsAPI.emptyListIsDisplayed();
    }

    @And("the status code is {int}")
    public void theStatusCodeIs(int statusCode) {
        productsAPI.verifyStatusCode(statusCode);
    }

    @Then("he sees the results that has the word {string} in the {string}")
    public void heSeesTheResultsThatHasTheWordInThe(String word, String key) {
        productsAPI.verifyResultsDisplayedForProductInResponseOnASpecificKey(word, key);
    }

    @Then("he sees the results returned for that product")
    public void heSeesTheResultsReturnedFromProduct() {
        productsAPI.verifyResultsDisplayedForProductInResponse();
    }
}
