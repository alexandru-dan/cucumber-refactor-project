Feature: Search for the products

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  Scenario: The user wants to search for different products that are available
    Given the user wants to use endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/" to get products
    When the user search for "apple"
    Then he sees the results returned for that product
    And the status code is 200
    When the user search for "cola"
    Then he sees the results returned for that product
    And the status code is 200
    When the user search for "orange"
    Then an empty list will be displayed
    And the status code is 200

  Scenario: The user wants to check that a product name is present in all the titles inside the response
    Given the user wants to use endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/" to get products
    When the user search for "apple"
    Then he sees the results that contains "apple" in the "title"
    And the status code is 200
    When the user search for "cola"
    Then he sees the results that contains "cola" in the "title"

   Scenario Outline: the user wants to search for products that aren't available
     Given the user wants to use endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/" to get products
     When the user search for '<product>'
     Then an error is displayed with the message "Not found"
     And the status code is 404

     Examples:
     |product|
     |nothing|
     |tron   |
     |features|

  Scenario: the user wants to use wrong endpoint
    Given the user wants to use endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/" to get products
    When the user search for "nothing"
    Then "Not authenticated" error is displayed
    And the status code is 401